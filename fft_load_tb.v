`timescale 1ns / 1ps

module fft_load_tb;

    /* inputs */
    reg clk, reset, enable;
    reg [1:0] source;
    reg [4:0] prn_satellite;
    reg baseband_mode;
    wire rf_data_valid;
    reg [1:0] I, Q;
    wire [7:0] fft_data_real, fft_data_imag;
    wire fft_data_valid;
    reg fft_data_ready;
    wire fft_data_last;
    wire ram_enable;
    wire [13:0] ram_address;
    reg [7:0] prn_ram_real, prn_ram_imag, sample_ram_real, sample_ram_imag;
    wire idle;

    fft_load uut(.clk(clk), .reset(reset), .enable(enable), .source(source),
                 .prn_satellite(prn_satellite),
                 /* baseband */
                 .baseband_mode(baseband_mode), .rf_data_valid(rf_data_valid), .I(I), .Q(Q),
                 /* fft */
                 .fft_data_real(fft_data_real), .fft_data_imag(fft_data_imag),
                 .fft_data_valid(fft_data_valid), .fft_data_ready(fft_data_ready),
                 .fft_data_last(fft_data_last),
                 /* rams */
                 .ram_enable(ram_enable), .ram_address(ram_address),
                 .prn_ram_real(prn_ram_real), .prn_ram_imag(prn_ram_imag),
                 .sample_ram_real(sample_ram_real), .sample_ram_imag(sample_ram_imag),
                 /* outputs */
                 .idle(idle));

    always #0.5 clk <= ~clk;

    /* baseband generator */
    integer base_count=0;
    always @(posedge clk)
    begin
        base_count <= (base_count+1)%3;

        //if(base_count==1)
            {Q, I} <= {Q, I} +1'b1;
    end
    assign rf_data_valid=(base_count==0);

    /* rams */
    always @(posedge clk)
    begin
        if(ram_enable)
        begin
            case(ram_address[1:0])
                2'b00:
                begin
                    prn_ram_real <= -8'd4;
                    prn_ram_imag <= 8'd18;
                    sample_ram_real <= 8'd89;
                    sample_ram_imag <= 8'd54;
                end
                2'b01:
                begin
                    prn_ram_real <= 8'd65;
                    prn_ram_imag <= -8'd5;
                    sample_ram_real <= 8'd23;
                    sample_ram_imag <= 8'd87;
                end
                2'b10:
                begin
                    prn_ram_real <= 8'd33;
                    prn_ram_imag <= 8'd62;
                    sample_ram_real <= -8'd21;
                    sample_ram_imag <= 8'd97;
                end
                2'b11:
                begin
                    prn_ram_real <= 8'd110;
                    prn_ram_imag <= 8'd17;
                    sample_ram_real <= 8'd7;
                    sample_ram_imag <= -8'd34;
                end
            endcase
        end
    end

    integer axi_counter=0;
    always @(posedge clk)
    begin
        if(idle | !reset)
            axi_counter <= 0;
        else if(fft_data_ready && fft_data_valid)
            axi_counter <= axi_counter + 1;

        if(axi_counter>16383)
            $display("%t: axi counter exceeded 16383 - %d", $time, axi_counter);
        if(fft_data_last)
            $display("%t: last asserted at %d - data ready %s  data valid  %s",
                     $time, axi_counter, fft_data_ready?"yes":"no", fft_data_valid?"yes":"no");
    end

    initial
    begin
        /* init inputs */
        clk=1;
        reset=1'b0;
        enable=1'b0;
        baseband_mode=1'b0;
        I=2'b0;
        Q=2'b0;

        #100;

        /* remove from reset */
        #1 reset=1;

        /* enable data receiving */
        fft_data_ready=1'b1;

        /* Baseband generation */
        #1 source=2'b00;
        enable=1'b1;
        #1 enable=1'b0;

        @(posedge idle);
        @(posedge clk);
        #10;

        /* PRN generation, satellite 13 */
        #1 source=2'b01;
        prn_satellite=5'd12;
        enable=1'b1;
        #1 enable=1'b0;

        @(posedge idle);
        @(posedge clk);
        #10;

        /* Multiplier with latency reduction */
        #1 source=2'b10;
        enable=1'b1;
        fft_data_ready=1'b0;
        #1 enable=1'b0;

        /* test latency reduction */
        #10 fft_data_ready=1'b1;

        /* test stalling */
        #7000 fft_data_ready=1'b0;
        #10 fft_data_ready=1'b1;

        @(posedge idle);
        @(posedge clk);
        #10;

        /* Multiplier */
        #1 source=2'b10;
        enable=1'b1;
        fft_data_ready=1'b1;
        #1 enable=1'b0;

        /* test stalling */
        #7000 fft_data_ready=1'b0;
        #10 fft_data_ready=1'b1;

        @(posedge idle);
        @(posedge clk);
        #10;
        $finish;
    end

endmodule
