module fft_load #(
    parameter NUM_CODES=1
    )
    (
    input clk,
    input reset,
    input enable,
    input [1:0] source, /* 00 - baseband, 01 - PRN, 1x - RAM multiplier */
    input [4:0] prn_satellite,
    /* baseband */
    input baseband_mode,
    input rf_data_valid,
    input [1:0] I, Q,
    /* fft module */
    output reg signed [7:0] fft_data_real, fft_data_imag,
    output reg fft_data_valid,
    input fft_data_ready,
    output reg fft_data_last,
    /* RAMs */
    output reg ram_enable,
    output [(13+NUM_CODES)-1:0] ram_address,
    input [7:0] prn_ram_real, prn_ram_imag,
    input [7:0] sample_ram_real, sample_ram_imag,
    /* idle output */
    output idle
    );

    /* PRN generator */
    reg ca_reset=1'b0, ca_enable=1'b0;
    wire ca_code;
    ca_gen_single prn(.clk(clk), .reset(ca_reset), .enable(ca_enable),
                      .sv_id(prn_satellite), .code(ca_code));

    /* baseband input & scaling */
    localparam BASEBAND_MODE_2BIT=0, BASEBAND_MODE_3BIT=1;
    reg signed [7:0] scaled_I, scaled_Q;
    always @*
    begin
        case(baseband_mode)
        BASEBAND_MODE_2BIT:
        begin
            case(I)
            2'b00: scaled_I = 8'd3;
            2'b01: scaled_I = 8'd7;
            2'b10: scaled_I = -8'd3;
            2'b11: scaled_I = -8'd7;
            endcase

            case(Q)
            2'b00: scaled_Q = 8'd3;
            2'b01: scaled_Q = 8'd7;
            2'b10: scaled_Q = -8'd3;
            2'b11: scaled_Q = -8'd7;
            endcase
        end

        BASEBAND_MODE_3BIT:
        begin
            case({I, Q[1]})
            3'b000: scaled_I = 8'd1;
            3'b001: scaled_I = 8'd3;
            3'b010: scaled_I = 8'd5;
            3'b011: scaled_I = 8'd7;
            3'b100: scaled_I = -8'd1;
            3'b101: scaled_I = -8'd3;
            3'b110: scaled_I = -8'd5;
            3'b111: scaled_I = -8'd7;
            endcase

            scaled_Q = scaled_I;
        end
        endcase
    end

    /* multiplier */
    wire signed [16:0] mul_unscaled_real, mul_unscaled_imag;
    reg mul_enable=1'b0;
    complex_mul mul(.clk(clk), .enable(mul_enable),
                    .a_real(prn_ram_real), .a_imag(prn_ram_imag),
                    .b_real(sample_ram_real), .b_imag(sample_ram_imag),
                    .out_real(mul_unscaled_real), .out_imag(mul_unscaled_imag));

    /* output scaling */
    reg signed [7:0] mul_scaled_real, mul_scaled_imag;
    localparam scaling_factor=1;
    /* FIXME scaling of 1 might be too little, scaling of 2 is too much - scaling of 1.5? */
    always @*
    begin
        if(mul_unscaled_real > ((127)<<<scaling_factor))
        begin
            /* synthesis translate_off */
            $display("%t: mul real +ve hard limit", $time);
            /* synthesis translate_on */
            mul_scaled_real = 8'd127;
        end
        else if(mul_unscaled_real < ((-127)<<<scaling_factor))
        begin
            /* synthesis translate_off */
            $display("%t: mul real -ve hard limit", $time);
            /* synthesis translate_on */
            mul_scaled_real = -8'd127;
        end
        else
            mul_scaled_real = mul_unscaled_real>>>scaling_factor;

        if(mul_unscaled_imag > ((127)<<<scaling_factor))
        begin
            /* synthesis translate_off */
            $display("%t: mul imag +ve hard limit", $time);
            /* synthesis translate_on */
            mul_scaled_imag = 8'd127;
        end
        else if(mul_unscaled_imag < ((-127)<<<scaling_factor))
        begin
            /* synthesis translate_off */
            $display("%t: mul imag -ve hard limit", $time);
            /* synthesis translate_on */
            mul_scaled_imag = -8'd127;
        end
        else
            mul_scaled_imag = mul_unscaled_imag>>>scaling_factor;
    end

    /* states */
    localparam STATE_IDLE=0, STATE_BASEBAND_LOAD=1, STATE_PRN_LOAD=2, STATE_ZERO_PAD=3,
               STATE_MUL_LOAD=4;
    reg [2:0] state=STATE_IDLE;

    assign idle=(state==STATE_IDLE)?1'b1:1'b0;

    /* data valid mux */
    reg fft_data_valid_int=1'b0;
    always @*
    begin
        if(state==STATE_BASEBAND_LOAD)
            fft_data_valid = rf_data_valid;
        else
            fft_data_valid = fft_data_valid_int;
    end

    /* output data mux */
    always @*
    begin
        case(state)
        STATE_BASEBAND_LOAD:
        begin
            fft_data_real = scaled_I;
            fft_data_imag = scaled_Q;
        end

        STATE_PRN_LOAD:
        begin
            if(ca_code)
                fft_data_real = 8'd7;
            else
                fft_data_real = -8'd7;
            fft_data_imag = 8'd0;
        end

        STATE_MUL_LOAD:
        begin
            fft_data_real = mul_scaled_real;
            fft_data_imag = mul_scaled_imag;
        end

        default:
        begin
            fft_data_real = 8'd0;
            fft_data_imag = 8'd0;
        end
        endcase
    end

    /* data counter */
    localparam PRN_END=(16368*NUM_CODES)-1;
    localparam BASEBAND_END=(16368*NUM_CODES)-1;
    localparam FFT_LEN=16384*NUM_CODES;
    localparam FFT_END=FFT_LEN-1;
    localparam MUL_LATENCY=2;
    localparam MUL_END=FFT_END + MUL_LATENCY;
    reg [13+NUM_CODES:0] data_counter;

    assign ram_address=data_counter[(13+NUM_CODES)-1:0];

    /* state machine */
    always @(posedge clk)
    begin
        if(!reset)
        begin
            state <= STATE_IDLE;

            ca_reset <= 1'b0;
            ca_enable <= 1'b0;

            mul_enable <= 1'b0;

            ram_enable <= 1'b0;

            fft_data_valid_int <= 1'b0;
            fft_data_last <= 1'b0;
        end
        else
        begin
            /* defaults */
            fft_data_valid_int <= 1'b0;
            fft_data_last <= 1'b0;
            ca_reset <= 1'b0;
            ca_enable <= 1'b0;
            mul_enable <= 1'b0;
            ram_enable <= 1'b0;

            case(state)
            STATE_IDLE:
            begin
                if(enable)
                begin
                    data_counter <= 'd0;

                    casez(source)
                    2'b00: state <= STATE_BASEBAND_LOAD;
                    2'b01: state <= STATE_PRN_LOAD;
                    2'b1?:
                    begin
                        state <= STATE_MUL_LOAD;
                        ram_enable <= 1'b1;
                    end
                    endcase
                end
            end

            STATE_BASEBAND_LOAD:
            begin
                /* we use a mux (above) so that rf_data_valid is connected directly
                 * to fft_data_valid - this saves adding buffering registers - so
                 * we only need to increment the counter when we see both rf_data_valid
                 * and fft_data_ready high
                 */
                if(rf_data_valid && fft_data_ready)
                begin
                    /* increment counter */
                    data_counter <= data_counter + 1'b1;

                    /* check end condition */
                    if(data_counter==BASEBAND_END)
                        state <= STATE_ZERO_PAD;
                end
                /* synthesis translate_off */
                if(rf_data_valid && (!fft_data_ready))
                    $display("%t: FFT loader baseband data loss (valid data, but FFT not ready", $time);
                /* synthesis translate_on */
            end

            STATE_PRN_LOAD:
            begin
                ca_reset <= 1'b1;
                fft_data_valid_int <= 1'b1;

                if(fft_data_ready)
                begin
                    /* code upsampling - only enable code generation every 16 samples */
                    if(data_counter[3:0]==4'b1111)
                        ca_enable <= 1'b1;

                    /* increment counter */
                    data_counter <= data_counter + 1'b1;

                    /* end condition */
                    if(data_counter==PRN_END)
                        state <= STATE_ZERO_PAD;
                end
            end

            STATE_ZERO_PAD:
            begin
                fft_data_valid_int <= 1'b1;

                if(fft_data_ready)
                begin
                    /* increment data_counter */
                    data_counter <= data_counter + 1'b1;

                    /* end condition */
                    if(data_counter==FFT_END)
                    begin
                        /* mark as last */
                        fft_data_last <= 1'b1;

                        /* return to idle */
                        state <= STATE_IDLE;
                    end
                end
            end

            STATE_MUL_LOAD:
            begin
                if(data_counter < MUL_LATENCY)
                begin
                    /* no valid data from multiplier */
                    mul_enable <= 1'b1;
                    ram_enable <= 1'b1;
                    /* add to counter as independent of fft */
                    data_counter <= data_counter + 1'b1;
                end
                else if(fft_data_ready)
                begin
                    /* mark data as valid */
                    fft_data_valid_int <= 1'b1;

                    /* enable the multiplier */
                    mul_enable <= 1'b1;

                    /* only allow reading of ram when below FFT end */
                    if(data_counter < FFT_END)
                        ram_enable <= 1'b1;

                    /* increment counter */
                    data_counter <= data_counter + 1'b1;

                    /* end condition */
                    if(data_counter==MUL_END)
                    begin
                        /* mark as last data point */
                        fft_data_last <= 1'b1;

                        /* return to idle */
                        state <= STATE_IDLE;
                    end
                end

                /* if data ready goes low, whilst data valid is high, we
                 * need to hold data valid high, otherwise the data that is
                 * currently being outputted will be lost
                 */
                if(fft_data_valid_int && (!fft_data_ready))
                    fft_data_valid_int <= 1'b1;
            end

            endcase
        end
    end

endmodule
