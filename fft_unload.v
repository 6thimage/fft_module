module fft_unload #(
    parameter NUM_CODES=1
    )
    (
    input clk,
    input reset,
    input enable,
    input mode, /* 0 - ready latch, 1 - peak detection */
    /* FFT IP */
    input signed [7:0] fft_data_real, fft_data_imag,
    input [(13+NUM_CODES)-1:0] fft_data_index,
    input fft_data_valid,
    output reg fft_data_ready,
    input fft_data_last,
    /* outputs */
    output reg [17:0] signal_peak,
    output reg [(13+NUM_CODES)-1:0] signal_peak_location,
    output reg [16:0] signal_mean,
    output reg signal_mean_overflow,
    output idle
    );

    /* states */
    localparam STATE_IDLE=0, STATE_TO_RL=1, STATE_TO_PEAK=2, STATE_PEAK_OUTPUT=3;
    reg [1:0] state=STATE_IDLE;

    assign idle=(state==STATE_IDLE)?1'b1:1'b0;

    /* only accept data from the FFT if we are in the to_RL or to_peak states */
    always @*
    begin
        case(state)
        STATE_TO_RL: fft_data_ready = 1'b1;
        STATE_TO_PEAK: fft_data_ready = 1'b1;
        default: fft_data_ready = 1'b0;
        endcase
    end

    /* peak detector */
    reg peak_reset;
    reg peak_enable;
    wire [(13+NUM_CODES)-1:0] peak_data_counter;
    wire [17:0] detect_peak;
    wire [13:0] detect_location;
    wire [16:0] detect_mean;
    wire detect_mean_overflow;
    wire peak_idle;
    peak_detect peak_detector(.clk(clk), .reset(peak_reset), .enable(peak_enable),
                              .fft_data_real(fft_data_real), .fft_data_imag(fft_data_imag),
                              .fft_data_index(fft_data_index),
                              .signal_peak(detect_peak), .peak_location(detect_location),
                              .signal_mean(detect_mean), .signal_mean_overflow(detect_mean_overflow),
                              .idle(peak_idle));

    always @*
    begin
        peak_enable = 1'b0;

        if(state==STATE_TO_PEAK)
            peak_enable = fft_data_valid;
    end

    /* remove peak detector from reset in to_peak and peak_output states */
    always @*
    begin
        case(state)
        STATE_TO_PEAK: peak_reset = 1'b1;
        STATE_PEAK_OUTPUT: peak_reset = 1'b1;
        default: peak_reset = 1'b0;
        endcase
    end

    always @(posedge clk)
    begin
        if(!reset)
        begin
            state <= STATE_IDLE;
        end
        else
        begin
            case(state)
            STATE_IDLE:
            begin
                if(enable)
                begin
                    case(mode)
                    1'b0: state <= STATE_TO_RL;
                    1'b1: state <= STATE_TO_PEAK;
                    endcase
                end
            end

            STATE_TO_RL:
            begin
                if(fft_data_last)
                    state <= STATE_IDLE;
            end

            STATE_TO_PEAK:
            begin
                if(fft_data_last)
                    state <= STATE_PEAK_OUTPUT;
            end

            STATE_PEAK_OUTPUT:
            begin
                if(peak_idle)
                begin
                    signal_peak <= detect_peak;
                    signal_peak_location <= detect_location;
                    signal_mean <= detect_mean;
                    signal_mean_overflow <= detect_mean_overflow;

                    state <= STATE_IDLE;
                end
            end

            endcase
        end
    end

endmodule

