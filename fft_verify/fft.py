#! /usr/bin/env python

import numpy as np
import pylab

ca=np.genfromtxt('../../soft_gps/gold_codes/codes/1', delimiter=',', dtype=np.int8)
# change [0,1] to [-1,1]
ca=ca*2 -1

# make 1024 long
ca=np.append(ca,ca[0])

# calculate fft
f_ca=np.fft.fft(ca)

# load from verilog sim
f_sim=np.genfromtxt('fft.data', dtype=np.complex64)

# scaling
f_ca=np.abs(f_ca)
f_ca=f_ca/np.max(f_ca)

f_sim=np.abs(f_sim)
f_sim=f_sim/np.max(f_sim)

# stats
print np.corrcoef(f_ca, f_sim)[0][1]
print np.min(f_ca), np.max(f_ca)
print np.min(f_sim), np.max(f_sim)


# plot
#pylab.plot(range(0,1024), f_sim/f_ca)
pylab.plot(range(0,1024), f_ca)
pylab.plot(range(0,1024), f_sim)
pylab.show()
