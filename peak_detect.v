module peak_detect(
    input clk, reset, enable,
    input signed [7:0] fft_data_real, fft_data_imag,
    input [13:0] fft_data_index,
    output reg [17:0] signal_peak,
    output reg [13:0] peak_location,
    output reg [16:0] signal_mean,
    output reg signal_mean_overflow,
    output idle
    );

    reg last_enable=1'b0;
    assign idle=((!enable) && (!last_enable))?1'b1:1'b0;

    reg [17:0] fft_abs_sq='d0;
    reg [13:0] signal_location='d0;

    always @(posedge clk)
    begin
        if(!reset)
        begin
            /* registers */
            last_enable <= 1'b0;
            fft_abs_sq <= 'd0;
            signal_location <= 'd0;

            /* outputs */
            signal_peak <= 'd0;
            peak_location <= 'd0;
            signal_mean <= 'd0;
            signal_mean_overflow <= 1'b0;
        end
        else
        begin
            last_enable <= enable;

            if(enable)
            begin
                /* calculate absolute of data & square it */
                fft_abs_sq <= (fft_data_real*fft_data_real) + (fft_data_imag*fft_data_imag);

                signal_location <= fft_data_index;
            end

            if(last_enable)
            begin
                /* peak detection */
                if(fft_abs_sq > signal_peak)
                begin
                    signal_peak <= fft_abs_sq;
                    peak_location <= signal_location;
                end

                /* signal mean */
                {signal_mean_overflow, signal_mean} <= signal_mean + (fft_abs_sq>>6/*/64*/);

                /* latching overflow flag */
                if(signal_mean_overflow)
                    signal_mean_overflow <= 1'b1;
            end
        end
    end

endmodule
