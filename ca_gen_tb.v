`timescale 1ns / 1ps

module ca_gen_tb;

    /* inputs */
    reg clk, reset, enable;
    /* outputs */
    wire [31:0] code;

    ca_gen uut(.clk(clk), .reset(reset), .enable(enable), .code(code));

    /* written out painfully as iverilog does not support generate statements */
    /*ca_gen #(.sv_id(1)) uut_0(.clk(clk), .reset(reset), .enable(enable), .code(code[0]));
    ca_gen #(.sv_id(2)) uut_1(.clk(clk), .reset(reset), .enable(enable), .code(code[1]));
    ca_gen #(.sv_id(3)) uut_2(.clk(clk), .reset(reset), .enable(enable), .code(code[2]));
    ca_gen #(.sv_id(4)) uut_3(.clk(clk), .reset(reset), .enable(enable), .code(code[3]));
    ca_gen #(.sv_id(5)) uut_4(.clk(clk), .reset(reset), .enable(enable), .code(code[4]));
    ca_gen #(.sv_id(6)) uut_5(.clk(clk), .reset(reset), .enable(enable), .code(code[5]));
    ca_gen #(.sv_id(7)) uut_6(.clk(clk), .reset(reset), .enable(enable), .code(code[6]));
    ca_gen #(.sv_id(8)) uut_7(.clk(clk), .reset(reset), .enable(enable), .code(code[7]));
    ca_gen #(.sv_id(9)) uut_8(.clk(clk), .reset(reset), .enable(enable), .code(code[8]));
    ca_gen #(.sv_id(10)) uut_9(.clk(clk), .reset(reset), .enable(enable), .code(code[9]));
    ca_gen #(.sv_id(11)) uut_10(.clk(clk), .reset(reset), .enable(enable), .code(code[10]));
    ca_gen #(.sv_id(12)) uut_11(.clk(clk), .reset(reset), .enable(enable), .code(code[11]));
    ca_gen #(.sv_id(13)) uut_12(.clk(clk), .reset(reset), .enable(enable), .code(code[12]));
    ca_gen #(.sv_id(14)) uut_13(.clk(clk), .reset(reset), .enable(enable), .code(code[13]));
    ca_gen #(.sv_id(15)) uut_14(.clk(clk), .reset(reset), .enable(enable), .code(code[14]));
    ca_gen #(.sv_id(16)) uut_15(.clk(clk), .reset(reset), .enable(enable), .code(code[15]));
    ca_gen #(.sv_id(17)) uut_16(.clk(clk), .reset(reset), .enable(enable), .code(code[16]));
    ca_gen #(.sv_id(18)) uut_17(.clk(clk), .reset(reset), .enable(enable), .code(code[17]));
    ca_gen #(.sv_id(19)) uut_18(.clk(clk), .reset(reset), .enable(enable), .code(code[18]));
    ca_gen #(.sv_id(20)) uut_19(.clk(clk), .reset(reset), .enable(enable), .code(code[19]));
    ca_gen #(.sv_id(21)) uut_20(.clk(clk), .reset(reset), .enable(enable), .code(code[20]));
    ca_gen #(.sv_id(22)) uut_21(.clk(clk), .reset(reset), .enable(enable), .code(code[21]));
    ca_gen #(.sv_id(23)) uut_22(.clk(clk), .reset(reset), .enable(enable), .code(code[22]));
    ca_gen #(.sv_id(24)) uut_23(.clk(clk), .reset(reset), .enable(enable), .code(code[23]));
    ca_gen #(.sv_id(25)) uut_24(.clk(clk), .reset(reset), .enable(enable), .code(code[24]));
    ca_gen #(.sv_id(26)) uut_25(.clk(clk), .reset(reset), .enable(enable), .code(code[25]));
    ca_gen #(.sv_id(27)) uut_26(.clk(clk), .reset(reset), .enable(enable), .code(code[26]));
    ca_gen #(.sv_id(28)) uut_27(.clk(clk), .reset(reset), .enable(enable), .code(code[27]));
    ca_gen #(.sv_id(29)) uut_28(.clk(clk), .reset(reset), .enable(enable), .code(code[28]));
    ca_gen #(.sv_id(30)) uut_29(.clk(clk), .reset(reset), .enable(enable), .code(code[29]));
    ca_gen #(.sv_id(31)) uut_30(.clk(clk), .reset(reset), .enable(enable), .code(code[30]));
    ca_gen #(.sv_id(32)) uut_31(.clk(clk), .reset(reset), .enable(enable), .code(code[31]));*/

    always #0.5 clk <= ~clk;

    initial
    begin
        /* init inputs */
        clk=1;
        reset=0;
        enable=0;

        /* remove from reset */
        #2;
        reset=1;

        /* start generation */
        #2;
        enable=1;

        #10;
        enable=0;

        #10;
        $finish;
    end

    reg [9:0] check_codes[0:31];

    reg [9:0] gen_codes[0:31];
    integer count=0, i, success=1;
    always @(posedge clk)
    begin
        if(reset && enable)
        begin
            for(i=0; i<32; i=i+1)
                gen_codes[i][9-count]=code[i];
            count=count+1;
        end

        if(count==10)
        begin
            for(i=0; i<32; i=i+1)
            begin
                if(gen_codes[i]==check_codes[i])
                    $display("SVD %2d %o %o", i, check_codes[i], gen_codes[i]);
                else
                begin
                    success=0;
                    $display("SVD %2d %o %o - mismatch", i, check_codes[i], gen_codes[i]);
                end
            end

            if(success)
                $display("\nTest successful");
            else
                $display("\nTest FAILED");
            $finish;
        end
    end

    initial
    begin
        /* IS-GPS-200H table 3-Ia sheet 1 pg 6 (19 in pdf) */
        check_codes[ 0]='o1440;
        check_codes[ 1]='o1620;
        check_codes[ 2]='o1710;
        check_codes[ 3]='o1744;
        check_codes[ 4]='o1133;
        check_codes[ 5]='o1455;
        check_codes[ 6]='o1131;
        check_codes[ 7]='o1454;
        check_codes[ 8]='o1626;
        check_codes[ 9]='o1504;
        check_codes[10]='o1642;
        check_codes[11]='o1750;
        check_codes[12]='o1764;
        check_codes[13]='o1772;
        check_codes[14]='o1775;
        check_codes[15]='o1776;
        check_codes[16]='o1156;
        check_codes[17]='o1467;
        check_codes[18]='o1633;

        /* IS-GPS-200H table 3-Ia sheet 2 pg 7 (20 in pdf) */
        check_codes[19]='o1715;
        check_codes[20]='o1746;
        check_codes[21]='o1763;
        check_codes[22]='o1063;
        check_codes[23]='o1706;
        check_codes[24]='o1743;
        check_codes[25]='o1761;
        check_codes[26]='o1770;
        check_codes[27]='o1774;
        check_codes[28]='o1127;
        check_codes[29]='o1453;
        check_codes[30]='o1625;
        check_codes[31]='o1712;
    end

endmodule
