module fft_module #(
    parameter NUM_CODES=1
    )
    (
    input clk,
    input reset,
    input enable,
    /* baseband */
    input baseband_mode,
    input rf_data_valid,
    input [1:0] I, Q,
    /* search parameters */
    input [13:0] rf_centre_frequency,
    input [5:0] doppler_side_shift,
    input [31:0] satellites,
    /* peak detection output */
    output reg satellite_valid,
    output reg [4:0] satellite_number,
    output reg signed [6:0] satellite_doppler,
    output reg [17:0] satellite_peak,
    output reg [(13+NUM_CODES)-1:0] satellite_peak_location,
    output reg [16:0] satellite_mean,
    output reg satellite_mean_overflow,
    output idle
    );

    localparam RAM_WIDTH=13+NUM_CODES;

    /* FFT IP */
    reg fft_reset=1'b0;
    /* config */
    reg fft_config_forward=1'b1, fft_config_valid=1'b0;
    wire fft_config_ready;
    /* data in */
    wire signed [7:0] fft_in_imag, fft_in_real;
    wire fft_in_valid, fft_in_last, fft_in_ready;
    /* data out */
    wire signed [7:0] fft_out_imag, fft_out_real;
    wire [23:0] fft_out_user; /* the user output contains multiple outputs padded to 8 bits */
    wire [4:0] fft_out_blk_exp=fft_out_user[20:16];
    wire [13:0] fft_out_xk_index=fft_out_user[13:0];
    wire fft_out_valid, fft_out_last, fft_out_ready;
    /* events */
    wire fft_frame_started, fft_last_unexpected, fft_last_missing;
    if(NUM_CODES==1)
        xfft_v8_0_16384 fft(
            .aclk(clk),
            .aresetn(fft_reset),
            /* config */
            .s_axis_config_tdata({7'd0, fft_config_forward}),
            .s_axis_config_tvalid(fft_config_valid),
            .s_axis_config_tready(fft_config_ready),
            /* data in */
            .s_axis_data_tdata({fft_in_imag, fft_in_real}),
            .s_axis_data_tvalid(fft_in_valid),
            .s_axis_data_tready(fft_in_ready),
            .s_axis_data_tlast(fft_in_last),
            /* data out */
            .m_axis_data_tdata({fft_out_imag, fft_out_real}),
            .m_axis_data_tuser(fft_out_user),
            .m_axis_data_tvalid(fft_out_valid),
            .m_axis_data_tready(fft_out_ready),
            .m_axis_data_tlast(fft_out_last),
            /* status */
            .m_axis_status_tdata(),
            .m_axis_status_tvalid(),
            .m_axis_status_tready(1'b1),
            /* events */
            .event_frame_started(fft_frame_started),
            .event_tlast_unexpected(fft_last_unexpected),
            .event_tlast_missing(fft_last_missing),
            .event_status_channel_halt(),
            .event_data_in_channel_halt(),
            .event_data_out_channel_halt()
            );
    else
        Invalid_Num_Codes fft();

    /* simulation event checking */
    /* synthesis translate_off */
    always @(posedge clk)
    begin
        if(fft_last_unexpected)
            $display("%t: fft_last_unexpected asserted", $time);
        if(fft_last_missing)
            $display("%t: fft_last_missing asserted", $time);
    end
    /* synthesis translate_on */

    /* RAMs
     * Both of the RAMs are dual-ported, with one port reading (connected to the loader)
     * and the other one writing (connected to the unloader).
     */
    wire ram_load_enable;
    wire [RAM_WIDTH-1:0] ram_load_address;
    /* PRN RAM */
    wire signed [7:0] prn_ram_load_real, prn_ram_load_imag;
    reg prn_ram_unload_enable;
    reg [RAM_WIDTH-1:0] prn_ram_unload_address;
    reg signed [7:0] prn_ram_unload_real, prn_ram_unload_imag;
    ram_dual_clock_dual_port #(.data_width(16), .address_width(RAM_WIDTH))
                                     /* A port read-only to FFT loader */
                             prn_ram(.A_clk(clk), .A_enable(ram_load_enable), .A_write_enable(1'b0),
                                         .A_address(ram_load_address), .A_data_in(16'd0),
                                         .A_data_out({prn_ram_load_imag, prn_ram_load_real}),
                                     /* B port write-only to FFT unloader */
                                     .B_clk(clk), .B_enable(prn_ram_unload_enable),
                                         .B_write_enable(1'b1), .B_address(prn_ram_unload_address),
                                         .B_data_in({prn_ram_unload_imag, prn_ram_unload_real}),
                                         .B_data_out());
    /* sample RAM */
    reg [RAM_WIDTH-1:0] rf_centre_frequency_int;
    reg signed [6:0] sample_ram_shift;
    reg [RAM_WIDTH-1:0] sample_ram_load_address;
    wire signed [7:0] sample_ram_load_real, sample_ram_load_imag;
    reg sample_ram_unload_enable;
    reg [RAM_WIDTH-1:0] sample_ram_unload_address;
    reg signed [7:0] sample_ram_unload_real, sample_ram_unload_imag;
    ram_dual_clock_dual_port #(.data_width(16), .address_width(RAM_WIDTH))
                                        /* A port read-only to FFT loader */
                             sample_ram(.A_clk(clk), .A_enable(ram_load_enable), .A_write_enable(1'b0),
                                            .A_address(sample_ram_load_address), .A_data_in(16'd0),
                                            .A_data_out({sample_ram_load_imag, sample_ram_load_real}),
                                        /* B port write-only to FFT unloader */
                                        .B_clk(clk), .B_enable(sample_ram_unload_enable),
                                            .B_write_enable(1'b1), .B_address(sample_ram_unload_address),
                                            .B_data_in({sample_ram_unload_imag,
                                                        sample_ram_unload_real}),
                                            .B_data_out());

    always @(*) sample_ram_load_address = ram_load_address + rf_centre_frequency_int + sample_ram_shift;

    /* FFT loader */
    reg loader_reset=1'b0, loader_enable=1'b0;
    localparam LOADER_SOURCE_BASEBAND=2'b00, LOADER_SOURCE_PRN=2'b01, LOADER_SOURCE_MUL=2'b10;
    reg [1:0] loader_source;
    reg [4:0] loader_satellite;
    wire loader_idle;
    fft_load #(.NUM_CODES(NUM_CODES))
             loader(.clk(clk), .reset(loader_reset), .enable(loader_enable), .source(loader_source),
                    .prn_satellite(loader_satellite),
                    /* baseband */
                    .baseband_mode(baseband_mode), .rf_data_valid(rf_data_valid), .I(I), .Q(Q),
                    /* FFT */
                    .fft_data_real(fft_in_real), .fft_data_imag(fft_in_imag),
                    .fft_data_valid(fft_in_valid), .fft_data_ready(fft_in_ready),
                    .fft_data_last(fft_in_last),
                    /* RAMs */
                    .ram_enable(ram_load_enable), .ram_address(ram_load_address),
                    .prn_ram_real(prn_ram_load_real), .prn_ram_imag(prn_ram_load_imag),
                    .sample_ram_real(sample_ram_load_real), .sample_ram_imag(sample_ram_load_imag),
                    /* outputs */
                    .idle(loader_idle));

    /* FFT unloader */
    reg unloader_reset=1'b0, unloader_enable=1'b0;
    localparam UNLOADER_MODE_LATCH=1'b0, UNLOADER_MODE_PEAK=1'b1;
    reg unloader_mode;
    wire [17:0] unloader_peak;
    wire [RAM_WIDTH-1:0] unloader_location;
    wire [16:0] unloader_mean;
    wire unloader_mean_overflow, unloader_idle;
    fft_unload #(.NUM_CODES(NUM_CODES))
               unloader(.clk(clk), .reset(unloader_reset), .enable(unloader_enable), .mode(unloader_mode),
                        /* FFT */
                        .fft_data_real(fft_out_real), .fft_data_imag(fft_out_imag),
                        .fft_data_index(fft_out_xk_index), .fft_data_valid(fft_out_valid),
                        .fft_data_ready(fft_out_ready), .fft_data_last(fft_out_last),
                        /* outputs */
                        .signal_peak(unloader_peak), .signal_peak_location(unloader_location),
                        .signal_mean(unloader_mean), .signal_mean_overflow(unloader_mean_overflow),
                        .idle(unloader_idle));

    /* RAM unloading mux */
    reg ram_unload_enable=1'b0;
    localparam RAM_UNLOAD_DESTINATION_SAMPLE=1'b0, RAM_UNLOAD_DESTINATION_PRN=1'b1;
    reg ram_unload_destination;
    always @*
    begin
        /* defaults */
        sample_ram_unload_enable = 1'b0;
        sample_ram_unload_address = 'd0;
        sample_ram_unload_real = 8'd0;
        sample_ram_unload_imag = 8'd0;
        prn_ram_unload_enable = 1'b0;
        prn_ram_unload_address = 'd0;
        prn_ram_unload_real = 8'd0;
        prn_ram_unload_imag = 8'd0;

        if(ram_unload_enable)
        begin
            case(ram_unload_destination)
            RAM_UNLOAD_DESTINATION_SAMPLE:
            begin
                sample_ram_unload_enable = fft_out_valid & fft_out_ready;
                sample_ram_unload_address = fft_out_xk_index;
                sample_ram_unload_real = fft_out_real;
                sample_ram_unload_imag = fft_out_imag;
            end

            RAM_UNLOAD_DESTINATION_PRN:
            begin
                prn_ram_unload_enable = fft_out_valid & fft_out_ready;
                prn_ram_unload_address = fft_out_xk_index;
                /* conjugate the PRN FFT */
                prn_ram_unload_real = fft_out_real;
                prn_ram_unload_imag = -fft_out_imag;
            end
            endcase
        end
    end

    /* states */
    localparam STATE_IDLE=0,             STATE_SATELLITE_CHECK=1, STATE_FFT_FWD_CONFIG=2,
               STATE_FFT_SAMPLE=3,       STATE_FFT_PRN_LOAD=4,    STATE_FFT_PRN_UNLOAD=5,
               STATE_FFT_PRN_COMPLETE=6, STATE_FFT_MUL_FIRST=7,   STATE_FFT_MUL_LOAD=8,
               STATE_FFT_MUL_UNLOAD=9;
    reg [3:0] state=STATE_IDLE;

    assign idle=(state==STATE_IDLE)?1'b1:1'b0;

    /* FFT reset */
    always @*
    begin
        case(state)
        STATE_FFT_FWD_CONFIG: fft_reset = 1'b1;
        STATE_FFT_SAMPLE: fft_reset = 1'b1;
        STATE_FFT_PRN_LOAD: fft_reset = 1'b1;
        STATE_FFT_PRN_UNLOAD: fft_reset = 1'b1;
        STATE_FFT_PRN_COMPLETE: fft_reset = 1'b1;
        STATE_FFT_MUL_FIRST: fft_reset = 1'b1;
        STATE_FFT_MUL_LOAD: fft_reset = 1'b1;
        STATE_FFT_MUL_UNLOAD: fft_reset = 1'b1;
        default: fft_reset = 1'b0;
        endcase
    end

    /* loader reset */
    always @*
    begin
        case(state)
        STATE_FFT_SAMPLE: loader_reset = 1'b1;
        STATE_FFT_PRN_LOAD: loader_reset = 1'b1;
        STATE_FFT_PRN_UNLOAD: loader_reset = 1'b1;
        STATE_FFT_PRN_COMPLETE: loader_reset = 1'b1;
        STATE_FFT_MUL_FIRST: loader_reset = 1'b1;
        STATE_FFT_MUL_LOAD: loader_reset = 1'b1;
        STATE_FFT_MUL_UNLOAD: loader_reset = 1'b1;
        default: loader_reset = 1'b0;
        endcase
    end

    /* unloader reset */
    always @*
    begin
        case(state)
        STATE_FFT_SAMPLE: unloader_reset = 1'b1;
        STATE_FFT_PRN_LOAD: unloader_reset = 1'b1;
        STATE_FFT_PRN_UNLOAD: unloader_reset = 1'b1;
        STATE_FFT_PRN_COMPLETE: unloader_reset = 1'b1;
        STATE_FFT_MUL_FIRST: unloader_reset = 1'b1;
        STATE_FFT_MUL_LOAD: unloader_reset = 1'b1;
        STATE_FFT_MUL_UNLOAD: unloader_reset = 1'b1;
        default: unloader_reset = 1'b0;
        endcase
    end

    /* internal registers */
    reg [5:0] doppler_side_shift_int;
    reg [31:0] satellites_int;
    reg [4:0] satellite_counter='d0;
    wire satellite_end=(satellite_counter=='d31)?1'b1:1'b0;
    reg sample_fetch, last_unload;
    reg fft_frame_started_latch=1'b0;
    reg signed [6:0] unloader_ram_shift;

    /* state machine */
    always @(posedge clk)
    begin
        if(!reset)
        begin
            state <= STATE_IDLE;

            fft_config_valid <= 1'b0;

            loader_enable <= 1'b0;
            unloader_enable <= 1'b0;
            ram_unload_enable <= 1'b0;

            fft_frame_started_latch <= 1'b0;
        end
        else
        begin
            /* default values */
            satellite_valid <= 1'b0;
            fft_config_valid <= 1'b0;
            loader_enable <= 1'b0;
            unloader_enable <= 1'b0;

            /* reset ram unloader on last bit */
            if(ram_unload_enable && fft_out_last)
                ram_unload_enable <= 1'b0;

            /* frame started latch */
            if(fft_frame_started || fft_frame_started_latch)
                fft_frame_started_latch <= 1'b1;

            case(state)
            STATE_IDLE:
            begin
                if(enable)
                begin
                    /* copy inputs to internal registers */
                    rf_centre_frequency_int <= rf_centre_frequency;
                    doppler_side_shift_int <= doppler_side_shift;
                    satellites_int <= satellites;

                    /* reset counter */
                    satellite_counter <= 'd0;

                    sample_fetch <= 1'b1;

                    /* move to next state */
                    state <= STATE_SATELLITE_CHECK;
                end
            end

            STATE_SATELLITE_CHECK:
            begin
                /* check if we are processing this satellite */
                if(satellites_int[0])
                begin
                    /* set bit to zero - this stops us processing it again */
                    satellites_int[0] <= 1'b0;

                    /* start processing */
                    state <= STATE_FFT_FWD_CONFIG;
                end
                else if(satellite_end || (satellites_int=='d0))
                    state <= STATE_IDLE;
                else
                begin
                    satellites_int <= satellites_int>>1;
                    satellite_counter <= satellite_counter + 1'b1;
                end
            end

            STATE_FFT_FWD_CONFIG:
            begin
                /* set FFT to forward */
                fft_config_forward <= 1'b1;
                fft_config_valid <= 1'b1;

                /* move to next state if IP is ready to receive config */
                if(fft_config_ready)
                    if(sample_fetch)
                        state <= STATE_FFT_SAMPLE;
                    else
                        state <= STATE_FFT_PRN_LOAD;
            end

            STATE_FFT_SAMPLE:
            begin
                /* reset sample fetch flag */
                sample_fetch <= 1'b0;

                /* enable loader and unloader */
                loader_source <= LOADER_SOURCE_BASEBAND;
                loader_enable <= 1'b1;
                unloader_mode <= UNLOADER_MODE_LATCH;
                unloader_enable <= 1'b1;
                ram_unload_destination <= RAM_UNLOAD_DESTINATION_SAMPLE;
                ram_unload_enable <= 1'b1;

                /* move to next state */
                state <= STATE_FFT_PRN_LOAD;
            end

            STATE_FFT_PRN_LOAD:
            begin
                if(loader_idle && (!loader_enable))
                begin
                    loader_source <= LOADER_SOURCE_PRN;
                    loader_satellite <= satellite_counter;
                    loader_enable <= 1'b1;

                    /* reset frame start latch */
                    fft_frame_started_latch <= 1'b0;

                    state <= STATE_FFT_PRN_UNLOAD;
                end
            end

            STATE_FFT_PRN_UNLOAD:
            begin
                /* we can reconfigure the FFT IP here, to do this we wait until the frame
                 * is being processed (so the configuration change doesn't apply to the
                 * current frame), which will cause the frame started latch to become high.
                 * When the IP accepts the configuration, we remove this latch condition.
                 *
                 * This does have the downside that there is no checking that the IP has
                 * accepted the new configuration - i.e. it is possible to enter and exit
                 * this state without the IP configuration being changed. This might be
                 * worth changing, but at the moment it shouldn't be a problem due to how
                 * long it takes to the FFT IP to process and load/unload.
                 *
                 * FIXME: possible non-change of FFT configuration
                 */
                if(fft_frame_started_latch)
                begin
                    fft_config_forward <= 1'b0;
                    fft_config_valid <= 1'b1;
                    if(fft_config_ready)
                        fft_frame_started_latch <= 1'b0;
                end

                /* TODO: condition could be changed to ram_unload_enable */
                if(unloader_idle && (!unloader_enable))
                begin
                    unloader_mode <= UNLOADER_MODE_LATCH;
                    unloader_enable <= 1'b1;
                    ram_unload_destination <= RAM_UNLOAD_DESTINATION_PRN;
                    ram_unload_enable <= 1'b1;

                    state <= STATE_FFT_PRN_COMPLETE;
                end
            end

            STATE_FFT_PRN_COMPLETE:
            begin
                /* as the following FFTs rely on the data from the sample and PRN FFTs
                 * we have to wait here until the PRN FFT has finished unloading
                 */
                /* TODO: condition could be changed to ram_unload_enable */
                if(unloader_idle && (!unloader_enable))
                    state <= STATE_FFT_MUL_FIRST;
            end

            STATE_FFT_MUL_FIRST:
            begin
                /* set up doppler search */
                sample_ram_shift <= -$signed(doppler_side_shift_int);
                /* start the first FFT off */
                loader_source <= LOADER_SOURCE_MUL;
                loader_enable <= 1'b1;
                unloader_mode <= UNLOADER_MODE_PEAK;
                unloader_enable <= 1'b1;

                if((!loader_idle) && (!unloader_idle))
                    state <= STATE_FFT_MUL_LOAD;
            end

            /*          0 1 2  3 4 5 6    7 8
             * loader   aaAA___bbBB____________
             * unloader aaaaaaaaaAA*_bbbbbBB*__
             *
             * above is roughly what happens, when configured for 2 peak detects to be run
             * (this can't happen, as the number is always odd, but it is a good example)
             *
             * 0 - the loader and unloader are configured to for the first data set (A)
             * 1 - dataset A is loaded into the FFT
             * 2 - loader becomes idle
             * 3 - loader configured for data set B
             * 4 - unload of A and load of B by FFT
             * 5 - peak output for data set A, unloader becomes idle
             * 6 - unloader configured for data set B
             * 7 - unload of B by FFT
             * 8 - peak output for data set B, unloader becomes idle
             *
             * prior to the loader becoming enabled (with the exception of the first unload),
             * we check to see if there is any more data to process
             *
             * the MUL_FIRST state produces the condition at 0 - both load and unloader enabled
             *
             * the doppler search goes from -ve side_shift to +ve side_shift, this makes the
             * logic a bit simpler (but probably slightly larger) and only requires an adder
             */

            STATE_FFT_MUL_LOAD:
            begin
                if(loader_idle && (!loader_enable))
                begin
                    /* store unloader shift */
                    unloader_ram_shift <= sample_ram_shift;

                    if(sample_ram_shift < $signed(doppler_side_shift_int))
                    begin
                        /* increment ram shift */
                        sample_ram_shift <= sample_ram_shift + 1'b1;

                        /* enabler loader */
                        loader_source <= LOADER_SOURCE_MUL;
                        loader_enable <= 1'b1;

                        /* mark that we have more loading to do */
                        last_unload <= 1'b0;
                    end
                    else
                        /* prevent further load / unload cycles */
                        last_unload <= 1'b1;

                    state <= STATE_FFT_MUL_UNLOAD;
                end
            end

            STATE_FFT_MUL_UNLOAD:
            begin
                if(unloader_idle && (!unloader_enable))
                begin
                    /* output peak info */
                    satellite_valid <= 1'b1;
                    satellite_number <= satellite_counter;
                    satellite_doppler <= unloader_ram_shift;
                    satellite_peak <= unloader_peak;
                    satellite_peak_location <= unloader_location;
                    satellite_mean <= unloader_mean;
                    satellite_mean_overflow <= unloader_mean_overflow;

                    /* if last_unload, this satellite's search is complete */
                    if(last_unload)
                        state <= STATE_SATELLITE_CHECK;
                    else
                    begin
                        /* otherwise, enable the unloader */
                        unloader_mode <= UNLOADER_MODE_PEAK;
                        unloader_enable <= 1'b1;

                        /* and return to the loading state */
                        state <= STATE_FFT_MUL_LOAD;
                    end
                end
            end

            endcase
        end
    end

endmodule

