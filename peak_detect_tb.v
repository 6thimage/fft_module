`timescale 1ns / 1ps

module peak_detect_tb;
    /* inputs */
    reg clk, reset, enable;
    reg signed [7:0] fft_data_real, fft_data_imag;
    reg [13:0] fft_data_counter;
    /* outputs */
    wire [17:0] signal_peak;
    wire [13:0] peak_location;
    wire [16:0] signal_mean;
    wire signal_mean_overflow;
    wire idle;

    peak_detect uut
                (.clk(clk), .reset(reset), .enable(enable),
                 .fft_data_real(fft_data_real), .fft_data_imag(fft_data_imag),
                 .fft_data_counter(fft_data_counter), .signal_peak(signal_peak),
                 .peak_location(peak_location), .signal_mean(signal_mean),
                 .signal_mean_overflow(signal_mean_overflow), .idle(idle));

    always #0.5 clk <= ~clk;

    integer data_gen_enable;

    always @(negedge clk)
    begin
        if(data_gen_enable)
            fft_data_counter <= fft_data_counter +1'b1;

        if(data_gen_enable==1)
        begin
            if(fft_data_counter==8261)
                fft_data_real <= 86;//fft_data_counter/1000;
            else
                fft_data_real <= 25;//fft_data_counter/100;
        end
        else if(data_gen_enable==2)
        begin
            fft_data_real <= 127;
            fft_data_imag <= 127;
        end
        else if(data_gen_enable==3)
        begin
            fft_data_real <= 1;
            fft_data_imag <= 0;
        end
    end

    initial
    begin
        /* init inputs */
        clk=1;
        reset=0;
        enable=0;
        fft_data_real=0;
        fft_data_imag=0;
        fft_data_counter=-1;

        #10;

        /* peak location test */
        reset=1;

        #2 data_gen_enable=1;
        #1 enable=1;

        #16384 data_gen_enable=0;
        enable=0;

        #5 reset=0;
        fft_data_counter=-1;
        #5;

        /* maximum signal mean */
        reset=1;

        #2 data_gen_enable=2;
        #1 enable=1;

        #16384 data_gen_enable=0;
        enable=0;

        #5 reset=0;
        fft_data_counter=-1;
        #5;

        /* minimum signal mean */
        reset=1;

        #2 data_gen_enable=3;
        #1 enable=1;

        #16384 data_gen_enable=0;
        enable=0;

        #10 $finish;
    end

endmodule
