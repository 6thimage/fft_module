`timescale 1ns / 1ps

module fft_unload_tb;
    reg clk;
    reg reset;
    reg enable;
    reg mode;
    reg signed [7:0] fft_data_real, fft_data_imag;
    wire [13:0] fft_data_index;
    reg fft_data_valid;
    wire fft_data_ready;
    reg fft_data_last;
    wire [17:0] signal_peak;
    wire [13:0] signal_peak_location;
    wire [16:0] signal_mean;
    wire signal_mean_overflow;
    wire idle;

    fft_unload uut(.clk(clk), .reset(reset), .enable(enable), .mode(mode),
                   .fft_data_real(fft_data_real), .fft_data_imag(fft_data_imag),
                       .fft_data_valid(fft_data_valid), .fft_data_ready(fft_data_ready),
                       .fft_data_last(fft_data_last),
                   .signal_peak(signal_peak), .signal_peak_location(signal_peak_location),
                       .signal_mean(signal_mean), .signal_mean_overflow(signal_mean_overflow),
                   .idle(idle)
                  );

    always #0.5 clk <= ~clk;

    integer axi_counter=0;
    integer axi_reset=0;
    always @(posedge clk)
    begin
        if(axi_reset)
            axi_counter <= 0;
        else if(fft_data_ready && fft_data_valid)
            axi_counter <= axi_counter + 1;
    end

    assign fft_data_index=axi_counter;

    always @(negedge clk)
    begin
        if(axi_counter==16382)
            fft_data_last <= 1;
        else
            fft_data_last <= 0;

        if(axi_counter==4298)
        begin
            fft_data_real <= 86;
            fft_data_imag <= -74;
        end
        else
        begin
            fft_data_real <= 25;
            fft_data_imag <= -19;
        end
    end

    initial
    begin
        /* initialise inputs */
        clk=1;
        reset=0;
        enable=0;

        #100;

        /* remove from reset */
        #1 reset=1;

        /* data for RAM */
        #1 mode=0;
        enable=1;
        #1 enable=0;
        #1.5 fft_data_valid=1;
        #0.5;

        #17000;

        #0.5 fft_data_valid=0;
        #0.5;

        #1000;

        if(axi_counter!=16383)
            $display("%t: axi counter does not equal 16383 - %d", $time, axi_counter);
        axi_reset=1;
        #1 axi_reset=0;

        /* peak detection */
        #1 mode=2;
        enable=1;
        #1 enable=0;
        #1.5 fft_data_valid=1;
        #0.5;

        #17000;

        #0.5 fft_data_valid=0;
        #0.5;

        #1000;

        if(axi_counter!=16383)
            $display("%t: axi counter does not equal 16383 - %d", $time, axi_counter);

        #10 $finish;
    end

endmodule
