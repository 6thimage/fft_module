module ram_dual_clock_dual_port
    #(
    parameter data_width=8,
    parameter address_width=2
    )
    (
    /* port A */
     input                         A_clk,
     input                         A_enable,
     input                         A_write_enable,
     input     [address_width-1:0] A_address,
     input     [data_width-1:0]    A_data_in,
    output reg [data_width-1:0]    A_data_out,
    /* port B */
     input                         B_clk,
     input                         B_enable,
     input                         B_write_enable,
     input     [address_width-1:0] B_address,
     input     [data_width-1:0]    B_data_in,
    output reg [data_width-1:0]    B_data_out
    );

    /* memory */
    reg [data_width-1:0] memory[(2**address_width)-1:0];

    /* default the memory to zero */
    integer i;
    initial
    begin
        for(i=0; i<(2**address_width); i=i+1)
            memory[i]='h0;
    end

    /* port A */
    always @(posedge A_clk)
    begin
        if(A_enable)
        begin
            /* write */
            if(A_write_enable)
                memory[A_address] <= A_data_in;

            /* read */
            A_data_out <= memory[A_address];
        end
    end

    /* port B */
    always @(posedge B_clk)
    begin
        if(B_enable)
        begin
            /* write */
            if(B_write_enable)
                memory[B_address] <= B_data_in;

            /* read */
            B_data_out <= memory[B_address];
        end
    end

endmodule
