#! /bin/bash

synth_log=fft_module_test.syr
line_count='wc -l'
proj_dir="/home/ian/programming/fpga/gps_receiver/fft_module/"

echo '*****************************************************************************************'
# limit to errors
errors=$(grep -i '^[ ]*error' $synth_log | sed 's/^[ ]*//g')
# remove directory
errors=$(echo "$errors" | sed "s|$proj_dir||g")

# output to screen
echo " Errors: $(if [ -z "$errors" ]; then echo -en '0'; else echo "$errors"|$line_count; fi)"
echo '*****************************************************************************************'
echo "$errors"

echo '*****************************************************************************************'
# limit to warnings
warnings=$(grep -i '^[ ]*warning' $synth_log | sed 's/^[ ]*//g')
# apply filtering
# remove directory
warnings=$(echo "$warnings" | sed "s|$proj_dir||g")
# remove warnings about ip/lpddr
warnings=$(echo "$warnings")

# output to screen
echo " Warnings: $(if [ -z "$warnings" ]; then echo -en '0'; else echo "$warnings"|$line_count; fi)"
echo '*****************************************************************************************'
echo "$warnings"

echo '*****************************************************************************************'
# limit to infos
infos=$(grep -i '^[ ]*info' $synth_log | sed 's/^[ ]*//g')
# apply filtering
# remove directory
infos=$(echo "$infos" | sed "s|$proj_dir||g")
# remove infos about ip/lpddr
infos=$(echo "$infos")

# output to screen
echo " Infos: $(if [ -z "$infos" ]; then echo -en '0'; else echo "$infos"|$line_count; fi)"
echo '*****************************************************************************************'
echo "$infos"

