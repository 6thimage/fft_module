#! /bin/bash

set -e

settings="settings.sh"

coregen_file=coregen.cgp

# source settings
if [ ! -e $settings ]; then
    echo "Error - settings file not at ($settings)"
    exit
fi
. $settings

# check environment
if [ ! -e $xilinx_settings ]; then
    echo "Error - Xilinx settings file not at ($xilinx_settings)"
    exit
fi
# source xilinx settings / environment
. $xilinx_settings &> /dev/null

# change to ip folder
cd $ip_folder

# launch coregen
coregen -p $coregen_file
