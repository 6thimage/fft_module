#! /bin/bash

set -e

settings="settings.sh"

sources="../fft_unload.v ../fft_unload_tb.v ../peak_detect.v"
top=fft_unload_tb

# source settings
if [ ! -e $settings ]; then
    echo "Error - settings file not at ($settings)"
    exit
fi
. $settings

# check environment
if [ ! -e $xilinx_settings ]; then
    echo "Error - Xilinx settings file not at ($xilinx_settings)"
    exit
fi
# source xilinx settings / environment
. $xilinx_settings &> /dev/null

# parse source
vlogcomp $sources

# create sim
fuse -o $top $top

# run sim
echo "run"|./$top -gui -view fft_unload.wcfg
#echo -en "onerror {resume}\nrun 1 s\n"|./$top

# remove sim & logs
[ -e $top ] && rm $top
[ -e isim.log ] && rm isim.log
[ -e isim.wdb ] && rm isim.wdb
[ -e fuse.log ] && rm fuse.log
[ -e fuse.xmsgs ] && rm fuse.xmsgs
[ -e fuseRelaunch.cmd ] && rm fuseRelaunch.cmd
[ -d isim ] && rm -r isim
