`timescale 1ns / 100ps

module fft_module_tb;
    reg clk, reset, enable, baseband_mode,  rf_data_valid=0;
    reg [1:0] I, Q;
    reg [13:0] rf_centre_frequency;
    reg [5:0] doppler_side_shift;
    //reg sample_mode;
    reg [31:0] satellites;
    wire satellite_valid;
    wire [4:0] satellite_number;
    wire signed [6:0] satellite_doppler;
    wire [17:0] satellite_peak;
    wire [13:0] satellite_peak_location;
    wire [16:0] satellite_mean;
    wire satellite_mean_overflow;
    wire idle;

    fft_module uut(.clk(clk),
                   .reset(reset),
                   .enable(enable),
                   .baseband_mode(baseband_mode),
                   .rf_data_valid(rf_data_valid),
                   .I(I),
                   .Q(Q),
                   .rf_centre_frequency(rf_centre_frequency),
                   .doppler_side_shift(doppler_side_shift),
                   //.sample_mode(sample_mode),
                   .satellites(satellites),
                   .satellite_valid(satellite_valid),
                   .satellite_number(satellite_number),
                   .satellite_doppler(satellite_doppler),
                   .satellite_peak(satellite_peak),
                   .satellite_peak_location(satellite_peak_location),
                   .satellite_mean(satellite_mean),
                   .satellite_mean_overflow(satellite_mean_overflow),
                   .idle(idle)
                   );

    always #0.5 clk <= ~clk;

    always @(posedge clk)
    begin
        if(satellite_valid)
        begin
            $display("%t: sat valid %d  dopp: %d  peak: %d  loc: %d  mean: %d  ovfl: %d",
                    $time, satellite_number, satellite_doppler, satellite_peak,
                    satellite_peak_location, satellite_mean, satellite_mean_overflow);
        end
    end

    /* baseband reader */
    reg baseband_enable=0;
    reg [7:0] data_byte;
    integer baseband_file, baseband_offset, read_status;
    always @(posedge baseband_enable)
    begin
        //baseband_file=$fopen("/home/ian/Desktop/gps_data/23jul15_1518_6mins_lna2_oldant_bboard.dat", "rb");
        baseband_file=$fopen("/media/ian/ext_gps_data/spirent_sim/static/leicester_gps_only_short_plus_9hrs.n3.dat",
                             "rb");
        if(!baseband_file)
            $finish;
        //if(baseband_offset!=0)
        //    read_status=$fseek(baseband_file, baseband_offset*16368/2, 0);
        read_status=$fseek(baseband_file, 512, 0);

        while(1)
        begin
            if(baseband_enable)
            begin
                read_status=$fread(data_byte, baseband_file);
                if(read_status!=1)
                    $display("baseband - read failed (%d)", data_byte);
                else
                begin
                    {I, Q} = data_byte[7:4];
                    rf_data_valid=1;
                    #1 rf_data_valid=0;
                    #2;

                    {I, Q} = data_byte[3:0];
                    rf_data_valid=1;
                    #1 rf_data_valid=0;
                    #2;
                end
            end
            else
                #1;
        end
    end

    initial
    begin
        /* init inputs */
        clk=1;
        reset=0;
        enable=0;

        #100;

        /* remove from reset */
        #2;
        reset=1;
        baseband_mode=0;

        /* start search */
        #2;
        rf_centre_frequency=4186;
        doppler_side_shift=7;
        //sample_mode=0;
        satellites='hffffffff;
        enable=1;

        #1 enable=0;

        #1000 baseband_enable=1;
        $display("%t: baseband enabled", $time);
        #50000 baseband_enable=0;
        $display("%t: baseband disabled", $time);

        /* second search */
        //#9000000 $finish;
        //enable=1;
        //#1 enable=0;

        /*#400000;
        satellites=2;
        enable=1;
        #1 enable=0;

        #400000;
        satellites=3;
        enable=1;
        #1 enable=0;*/
    end

endmodule
