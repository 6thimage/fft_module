module ca_gen_single(
    input clk,
    input reset,
    input enable,
    input [4:0] sv_id,
    output code
    );

    reg [9:0] G1={10{1'b1}}, G2={10{1'b1}};

    reg prn_taps;

    assign code=prn_taps^G1[9];

    always @(posedge clk)
    begin
        if(!reset)
        begin
            {G1, G2} <= {10'h3ff, 10'h3ff};
        end
        else if(enable)
        begin
            G1 <= {G1[8:0], G1[2]^G1[9]};
            G2 <= {G2[8:0], G2[1]^G2[2]^G2[5]^G2[7]^G2[8]^G2[9]};
        end

    end

    always @*
    begin
        case(sv_id)
            /* IS-GPS-200H table 3-Ia sheet 1 pg 6 (19 in pdf) */
            0:  prn_taps=G2[1]^G2[5];
            1:  prn_taps=G2[2]^G2[6];
            2:  prn_taps=G2[3]^G2[7];
            3:  prn_taps=G2[4]^G2[8];
            4:  prn_taps=G2[0]^G2[8];
            5:  prn_taps=G2[1]^G2[9];
            6:  prn_taps=G2[0]^G2[7];
            7:  prn_taps=G2[1]^G2[8];
            8:  prn_taps=G2[2]^G2[9];
            9:  prn_taps=G2[1]^G2[2];
            10: prn_taps=G2[2]^G2[3];
            11: prn_taps=G2[4]^G2[5];
            12: prn_taps=G2[5]^G2[6];
            13: prn_taps=G2[6]^G2[7];
            14: prn_taps=G2[7]^G2[8];
            15: prn_taps=G2[8]^G2[9];
            16: prn_taps=G2[0]^G2[3];
            17: prn_taps=G2[1]^G2[4];
            18: prn_taps=G2[2]^G2[5];

            /* IS-GPS-200H table 3-Ia sheet 2 pg 7 (20 in pdf) */
            19: prn_taps=G2[3]^G2[6];
            20: prn_taps=G2[4]^G2[7];
            21: prn_taps=G2[5]^G2[8];
            22: prn_taps=G2[0]^G2[2];
            23: prn_taps=G2[3]^G2[5];
            24: prn_taps=G2[4]^G2[6];
            25: prn_taps=G2[5]^G2[7];
            26: prn_taps=G2[6]^G2[8];
            27: prn_taps=G2[7]^G2[9];
            28: prn_taps=G2[0]^G2[5];
            29: prn_taps=G2[1]^G2[6];
            30: prn_taps=G2[2]^G2[7];
            31: prn_taps=G2[3]^G2[8];

            default: prn_taps=1'b0;
        endcase
    end

endmodule

