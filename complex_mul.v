module complex_mul(
    input clk, enable,
    input signed [7:0] a_real, a_imag, b_real, b_imag,
    output signed [16:0] out_real, out_imag
    );

    /* 3 multipliers - requires 3 DSP48A1s, ~ 117.6 MHz, 2 cycle latency */
    reg signed [7:0] stage0_ar, stage0_ai, stage0_bi;
    reg signed [8:0] stage0_s1, stage0_s2, stage0_a1;
    reg signed [16:0] stage1_pr, stage1_pi;

    assign out_real=stage1_pr;
    assign out_imag=stage1_pi;

    always @(posedge clk)
    begin
        if(enable)
        begin
            stage0_ar <= a_real;
            stage0_ai <= a_imag;
            stage0_bi <= b_imag;
            stage0_s1 <= a_real - a_imag;
            stage0_s2 <= b_real - b_imag;
            stage0_a1 <= b_real + b_imag;

            stage1_pr <= (stage0_s1*stage0_bi) + (stage0_s2*stage0_ar);
            stage1_pi <= (stage0_s1*stage0_bi) + (stage0_a1*stage0_ai);
        end
    end/**/

    /* 4 multipliers - requires 4 DSP48A1s, ~ 456.6 MHz, 3 cycle latency *
    reg signed [7:0] stage0_ar, stage0_br, stage0_ai, stage0_bi;
    reg signed [7:0] stage1_ar, stage1_br, stage1_bi;
    reg signed [15:0] stage1_m1, stage1_m2;
    reg signed [15:0] stage2_m1, stage2_m2, stage2_i1, stage2_n1;
    reg signed [16:0] stage3_pr, stage3_pi;

    assign out_real=stage3_pr;
    assign out_imag=stage3_pi;

    always @(posedge clk)
    begin
        if(enable)
        begin
            stage0_ar <= a_real;
            stage0_br <= b_real;
            stage0_ai <= a_imag;
            stage0_bi <= b_imag;

            stage1_ar <= stage0_ar;
            stage1_br <= stage0_br;
            stage1_m1 <= stage0_ai * stage0_bi;

            stage1_bi <= stage0_bi;
            stage1_m2 <= stage0_ai * stage0_br;

            stage2_m1 <= stage1_ar * stage1_br;
            stage2_i1 <= -stage1_m1;

            stage2_m2 <= stage1_ar * stage1_bi;
            stage2_n1 <= stage1_m2;

            stage3_pr <= stage2_m1 + stage2_i1;
            stage3_pi <= stage2_m2 + stage2_n1;
        end
    end/**/

endmodule

