`timescale 1ns / 1ps

module complex_mul_tb;

    /* inputs */
    reg clk, enable;

    reg signed [7:0] a_real, a_imag, b_real, b_imag;
    wire signed [16:0] out_real, out_imag;

    complex_mul uut(.clk(clk), .enable(enable),
                    .a_real(a_real), .a_imag(a_imag), .b_real(b_real),
                    .b_imag(b_imag), .out_real(out_real), .out_imag(out_imag));

    always #0.5 clk <= ~clk;

    initial
    begin
        /* init inputs */
        clk=1;
        enable=1;
        a_real=0;
        a_imag=0;
        b_real=0;
        b_imag=0;

        #10;

        a_real=8'd1; a_imag=-8'd1;
        b_real=8'd2; b_imag=8'd3;

        #20 $finish;
    end

endmodule
